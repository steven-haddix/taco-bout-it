const https = require('https');
const querystring = require('querystring');
const AWS = require('aws-sdk');
const { type } = require('os');
AWS.config.update({ region: process.env.AWS_REGION });


async function apiCall(params) {
    return new Promise((resolve, reject) => {
        https.get(process.env.SLACK_PROFILE_URL + '?' + params, (res) => {
            res.on('data', (d) => {
                resolve(d)
            });
        }).on('error', (e) => {
            reject(e)
        })
    })
}

Date.prototype.getWeekNumber = function(){
    const d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    const dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    const yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
};

function call(action, params) {
    const dynamoDb = new AWS.DynamoDB.DocumentClient();
    return dynamoDb[action](params).promise();
}

async function getUsers(userId) {
    const date = new Date();

    return call('query', {
        TableName: "TacoBoutIt-" + process.env.STAGE + "-users",
        IndexName: 'weekKey-tacoCount-index',
        KeyConditionExpression: 'tacoCount > :tacoCount AND weekKey = :weekKey',
        ExpressionAttributeValues: {
            ':tacoCount': -1000,
            ':weekKey': parseInt(date.getFullYear() + '' + date.getWeekNumber()),
        },
        ScanIndexForward: false
    });
}

async function getSlackUser(user) {
    const params = {
        token: process.env.SLACK_TOKEN,
        user: user.slackId
    }
    const searchParams = Object.keys(params).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    }).join('&');

    try {
        const res = await apiCall(searchParams);
        let profile = JSON.parse(res);

        return {
            ...user,
            ...{
                name: profile.user.real_name
            }
        };
    } catch(error) {
        console.log(error)
        return {
            ...user,
            ...{
                name: 'unavailable',
            }
        }
    }
}

async function getSlackUsers(users) {
    const userPromises = users.map(async (user) => {
        try {
            return await getSlackUser(user)
        } catch(err) {
            console.log('Unable get Slack information for user', user);
            console.log(err);
        }
    });

    return Promise.all(userPromises);
}

const done = (err, res, callback) => {
    // Required since serverless doesn't pass callback on deployment errors
    if (typeof callback !== 'function') {
        return;
    }

    callback(null, {
        statusCode: "200",
        body: err ? JSON.stringify({ error: err.message }) : JSON.stringify(res),
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "application/json"
        }
    });
}

async function handler(event, context, callback) {
    try {
        const res = await getUsers();
        const users = res.Items;
        const slackUsers = await getSlackUsers(users);

        return done(null, { users: slackUsers }, callback);
    } catch(ex) {
        console.error(ex);
        return done(ex.message, null, callback);
    }
};

exports.apiCall = apiCall;
exports.handler = handler;
