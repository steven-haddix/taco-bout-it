'use strict';
const AWSMock = require('aws-sdk-mock');
const AWS = require('aws-sdk');

const https = require('https')
jest.mock('https')

const mod = require('./users');
const jestPlugin = require('serverless-jest-plugin');
const lambdaWrapper = jestPlugin.lambdaWrapper;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' });

describe('users', () => {
  beforeEach(() => {
    process.env.AWS_REGION = 'us-east-1'
    jest.setTimeout(10000);
  })

  it('returns user database recrods with correctly merged real name', () => {
    // TODO: move this to utility
    https.get = jest.fn((url, cb) => cb({
      on: (data, cb) => cb(JSON.stringify({user: {real_name: 'steven'}}))
    }))

    AWSMock.setSDKInstance(AWS);
    AWSMock.mock('DynamoDB.DocumentClient', 'query', (params, callback) => {
      callback(null, { Items: [
          { slackId: 1 }
        ]});
    });

    return wrapped.run().then((response) => {
      const parsedResponse = JSON.parse(response.body)
      expect(parsedResponse.users[0].slackId).toEqual(1);
      expect(parsedResponse.users[0].name).toEqual('steven');
    });
  });
});
