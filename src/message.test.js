'use strict';

const mod = require('./message');

const jestPlugin = require('serverless-jest-plugin');
const lambdaWrapper = jestPlugin.lambdaWrapper;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' });

describe('message handler', () => {
  it('returns correct challenge code when type when challenge is present', () => {
    const body = {
      body: JSON.stringify({
        "token":"BE7xP3swZtUBQrwWDdozjhKT",
        "challenge":"odpEjl9cJoOEeHjlGj1R0SqDOISmRtuQbfTQ3ocQEDPLHcJNOuVj",
        "type":"url_verification"
      })
    }

    return wrapped.run(body).then((response) => {
      expect(response.statusCode).toEqual(200);
      expect(JSON.parse(response.body).challenge).toEqual("odpEjl9cJoOEeHjlGj1R0SqDOISmRtuQbfTQ3ocQEDPLHcJNOuVj")
    })
  });

  it('returns nothing and prevents additional calls when bot chit chat', () => {
    const body = {
      body: JSON.stringify({
        "event": {
          "subtype": "bot_message"
        }
      })
    }

    return wrapped.run(body).then((response) => {
      expect(response.statusCode).toEqual(200);
      expect(JSON.parse(response.body)).toEqual({})
    })
  });
});