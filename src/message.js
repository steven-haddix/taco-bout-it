const https = require('https');
const querystring = require('querystring');
const AWS = require('aws-sdk');

const table = "TacoBoutIt-" + process.env.STAGE + "-users";
const tableEvents = "TacoBoutIt-" + process.env.STAGE + "-events";

Date.prototype.getWeekNumber = function(){
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
};

function call(action, params) {
    const dynamoDb = new AWS.DynamoDB.DocumentClient();
    return dynamoDb[action](params).promise();
}

async function apiCall(data) {
    const payload = JSON.stringify(data);
    let responseString = "";

    return new Promise((resolve, reject) =>  {
        var options = {
            hostname: 'hooks.slack.com',
            port: 443,
            path: process.env.SLACK_HOOK,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            }
        };

        var req = https.request(options, (res) => {
            res.setEncoding('utf8');

            res.on('data', (d) => {
                resolve(d)
            });
        });

        req.on('error', (e) => {
            reject(e)
        });

        req.write(payload);
        req.end();
    })
}

function getTableKey(userId) {
    const date = new Date();
    return  userId + '-' + date.getWeekNumber() + '-' + date.getFullYear()
}

async function getUser(userId) {
    return call('get', {
        TableName: table,
        Key: {
            userId: getTableKey(userId)
        },
    });
}

async function createUser(userId, tacoCount) {
    const date = new Date();

    return call('put', {
        TableName: table,
        Item: {
            userId: getTableKey(userId),
            slackId: userId,
            tacoCount: tacoCount ? tacoCount : 1,
            partitionKey: 1,
            weekKey: parseInt(date.getFullYear() + '' + date.getWeekNumber()),
        }
    });
}

async function updateUserCount(userId, count) {
    return call('update', {
        TableName: table,
        Key: {
            userId: getTableKey(userId)
        },
        UpdateExpression: 'SET tacoCount = :tacoCount',
        ExpressionAttributeValues: {
            ':tacoCount': count
        },
        ReturnValues: 'ALL_NEW',
    })
}

async function createEvent(giverId, receiverId, tacoCount, message) {
    const now = new Date();
    const eventId = giverId + '_' + new Date().toISOString().replace(' ', '_').replace(/\./g, '-');

    return call('put', {
        TableName: tableEvents,
        Item: {
            eventId: eventId,
            giverId,
            receiverId,
            tacoCount,
            message
        }
    });
}

const done = (err, res) => new Promise((resolve, reject) => {
    resolve({
        statusCode: 200,
        body: err ? JSON.stringify({ error: err.message }) : JSON.stringify(res),
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "application/json"
        }
    })
})

async function handler(event, context, callback) {
    try {
        const body = JSON.parse(event.body);

        if (body.challenge) {
            return done(null, { "challenge": body.challenge });
        }

        if (body.event.subtype === 'bot_message') {
            console.log('ignoring bot chit-chat')
            return done(null, {});
        }

        const tacoMentions = body.event.text.match(/\:taco\:/g);
        const userMention = body.event.text.match(/<@(.*?)>/);

        // Taco handler needs a person and a taco emoji to be triggered
        if (tacoMentions.length < 1 || userMention < 1) {
            return done(null, {});
        }

        const giver = body.event.user;
        const receiverSlack = userMention[0];
        const receiverId = userMention[1];

        try {
            const res = await getUser(receiverId);
            const user = res.Item;

            if (user) {
                await updateUserCount(receiverId, user.tacoCount + tacoMentions.length);
            } else {
                await createUser(receiverId, tacoMentions.length);
            }

            const message = '<@' + giver + '> just gave ' + receiverSlack + ' a taco! 🌮';
            await apiCall({text: message});
            // await createEvent(giver, receiverId, tacoMentions.length, body.event.text);
            return done(null, { ok: true })
        } catch(err) {
            console.log(err)
            return done('Bad request body')
        }

    } catch(ex) {
        console.log(event);
        console.log(ex);
        return done('Bad request body')
    }
};

exports.handler = handler;