const https = require("https");

async function apiCall(data) {
    return new Promise((resolve, reject) => {
        var options = {
            hostname: "slack.com",
            port: 443,
            path: "/api/users.info",
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
        var req = https.request(options, res => {
            res.setEncoding("utf8");

            // temporary data holder
            const body = [];

            // on every content chunk, push it to the data array
            res.on("data", chunk => body.push(chunk));

            // we are done, resolve promise with those joined chunks
            res.on("end", () => resolve(body.join("")));
        });

        req.on("error", e => {
            reject(e);
        });

        req.write(data);
        req.end();
    });
}

async function handler(event, context, callback) {
    const done = (err, res) =>
        callback(null, {
            statusCode: "200",
            body: err ? JSON.stringify({ error: err.message }) : JSON.stringify(res),
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Content-Type": "application/json"
            }
        });


    const searchParams = Object.keys(event.queryStringParameters)
        .map(key => {
            return (
                encodeURIComponent(key) +
                "=" +
                encodeURIComponent(event.queryStringParameters[key])
            );
        })
        .join("&");

    try {
        const res = await apiCall(searchParams);

        let profile = JSON.parse(res);

        return done(false, profile);
    } catch(error) {
        return done(error, {
            name: 'unavailable',
        });
    }


}

exports.handler = handler;
